# Prisoner's dilemma model - now in 3D.
![screen](screen.PNG)

## What it is
- Implementation of [NetLogo model of Prisoner's dilemma](https://ccl.northwestern.edu/netlogo/models/PDBasicEvolutionary) in 3D space
- Simple JavaScript/WebGL2 application using only [THREE.js](https://threejs.org/) (WebGL wrapper) and [dat.GUI](https://github.com/dataarts/dat.gui) (GUI library) which are already part of the repository
- The whole model is computed on the GPU (GLSL shaders)

## How to run it
- WebGL2 compatible browser - [check here](https://get.webgl.org/webgl2/)
- Navigate to [http://prisoner-dilemma-3d.surge.sh/](http://prisoner-dilemma-3d.surge.sh/)

## How to run it locally
- Application [must be served from an HTTP server](https://stackoverflow.com/a/50197517/5574297) as the codebase is included inside the index.html as a JavaScript module
  - i.e. simple python http server serving static content `python -m http.server 8080` and then navigate to `http://localhost:8080`

## How to use it
- Use mouse to rotate the model
- Scroll to zoom in/out the model
- Use sliders in the upper right corner to tweak the configuration
- Check the 'Running' checkbox to start the model

## Other
- Why JavaScript?
  - The project was originally implemented using OpenGL/JOGL, but I wanted to "just run it anywhere" - OpenGL implementation (even when using JOGL on the JVM) tends to be OS specific
- Why no webpack/bundling/minification? Then there would be no need to serve the static content locally as it could be bundled and accessed directly from filesystem
  - Who wants to spend their life configuring unnecessary convoluted JavaScript frameworks/libraries?
