export const COMPUTE_VERTEX_SHADER = `
void main() {
    gl_Position = vec4( position.xy, 0.0, 1.0 );
}
`;

const CONVERT_2D_3D_TEXTURE_UTILS = `
uniform int verticesPerEdge;
uniform sampler2D nodes;

vec3 getCurrentNode() {
    return texelFetch(nodes, ivec2(gl_FragCoord.xy), 0).xyz;
}

vec3 getNode(int x, int y, int z) {
    int slices = int(sqrt(float(verticesPerEdge)));
    ivec2 src2D = ivec2(
        x + (z % slices) * verticesPerEdge,
        y + (z / slices) * verticesPerEdge
    );
    return texelFetch(nodes, src2D, 0).xyz;
}

ivec3 getCurrentPos3D() {
    int slices = int(sqrt(float(verticesPerEdge)));
    ivec2 currentPos2D = ivec2(gl_FragCoord.xy);
    return ivec3(
        currentPos2D.x % verticesPerEdge,
        currentPos2D.y % verticesPerEdge,
        currentPos2D.x / verticesPerEdge + (currentPos2D.y / verticesPerEdge) * slices
    );
}

int wrapWorldCoordIfNeeded(int coord) {
    if (coord < 0) {
        return verticesPerEdge - 1;
    } else if (coord >= verticesPerEdge) {
        return 0;
    } else {
        return coord;
    }
}

void getNeighbors(inout vec3 neighbors[26]) {
    ivec3 c = getCurrentPos3D();

    int index = 0;
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            for (int k = -1; k <= 1; k++) {
                if (i != 0 || j != 0 || k != 0) { // skip itself
                    neighbors[index++] = getNode(
                        wrapWorldCoordIfNeeded(c.x + i),
                        wrapWorldCoordIfNeeded(c.y + j),
                        wrapWorldCoordIfNeeded(c.z + k)
                    );
                }
            }
        }
    }
}
`;

export const INTERACT_FRAGMENT_SHADER =
  CONVERT_2D_3D_TEXTURE_UTILS +
  `
uniform float defectionAward;

float countCooperatingNeighbors() {
    vec3 neighbors[26];
    getNeighbors(neighbors);

    float score = 0.0;
    for (int i = 0; i < 26; i++) {
        score += neighbors[i].x;
    }
    
    return score;
}

void main() {
    vec3 currentNode = getCurrentNode();
    float score = countCooperatingNeighbors();
    if (currentNode.x != 1.0) { // not cooperating -> apply award for defection
        score *= defectionAward;
    }
    float maxScore = 26.0 * defectionAward; // score must be normalized between <0,1> as only values <0,1> can be rendered into the texture
    gl_FragColor = vec4(currentNode.xy, score / maxScore, 0.0);
}
`;

export const SELECT_STRATEGY_FRAGMENT_SHADER =
  CONVERT_2D_3D_TEXTURE_UTILS +
  `
uniform bool running;

vec3 selectNeighborWithHighestScore() {
    vec3 neighbors[26];
    getNeighbors(neighbors);

    vec3 neighborWithHighestScore = vec3(0.0);
    for (int i = 0; i < 26; i++) {
        vec3 neighbor = neighbors[i];
        if (neighbor.z > neighborWithHighestScore.z) {
            neighborWithHighestScore = neighbor;
        }
    }

    return neighborWithHighestScore;
}

void main() {
    vec3 currentNode = getCurrentNode();
    if (running) {
        vec3 neighborWithHighestScore = selectNeighborWithHighestScore();
        gl_FragColor = vec4(neighborWithHighestScore.x, currentNode.x, currentNode.z, 0.0);
    } else {
        gl_FragColor = vec4(currentNode, 0.0);
    }
}
`;

export const RENDER_VERTEX_SHADER = `
uniform int verticesPerEdge;
uniform float pointsSize;

flat out ivec3 texcoord;

void main() {
	gl_PointSize = pointsSize;
	texcoord = ivec3(position.xyz);
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position / (float(verticesPerEdge) - 1.0), 1.0 );
}
`;

export const RENDER_FRAGMENT_SHADER =
  CONVERT_2D_3D_TEXTURE_UTILS +
  `
flat in ivec3 texcoord;

void main() {
    vec3 currentNode = getNode(texcoord.x, texcoord.y, texcoord.z);
    
    float currentState = currentNode.x;
    float previousState = currentNode.y;
    if ( currentState == previousState ) {
        if ( currentState != 1.0 ) { // defect
            gl_FragColor = vec4( 0.843, 0.196, 0.160, 1.0 );
        } else { // cooperate
            gl_FragColor = vec4( 0.203, 0.364, 0.662, 1.0 );
        }
    } else if ( currentState != 1.0 ) { // cooperate -> defect
        gl_FragColor = vec4( 0.349, 0.690, 0.235, 1.0 );
    } else { // defect -> cooperate
        gl_FragColor = vec4( 0.929, 0.929, 0.192, 1.0 );
    }
}
`;
