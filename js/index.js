import { init, render } from "./renderer.js";

function animate() {
  requestAnimationFrame(animate);
  render();
}

init();
animate();
