import * as THREE from "./threejs/Three.js";
import { OrbitControls } from "./threejs/OrbitControls.js";

import { getState } from "./state.js";
import {
  COMPUTE_VERTEX_SHADER,
  INTERACT_FRAGMENT_SHADER,
  SELECT_STRATEGY_FRAGMENT_SHADER,
  RENDER_FRAGMENT_SHADER,
  RENDER_VERTEX_SHADER,
} from "./shaders.js";

let renderTarget;
let noopCamera;
let sceneInteract, objectInteract;
let sceneStrategy, objectStrategy;
let scene, object, camera, controls, canvasRenderer;

let textureBuffer;

const state = getState({
  onPointsSizeChange: onPointsSizeChange,
  onDefectionAwardChange: onDefectionAwardChange,
  onRunningChange: onRunningChange,
  onVerticesPerEdgeChange: init,
});

export function init() {
  setRenderer();
  setCamera();
  setRtt();
  setScene();

  textureBuffer = new Uint8Array(Math.pow(state.verticesPerEdge, 3) * 4);

  let element = document.getElementById("container");
  element.textContent = "";
  element.appendChild(canvasRenderer.domElement);
  window.addEventListener("resize", onWindowResize, false);
}

export function render() {
  if (state.running) {
    state.ticks++;
  }

  // nodes interaction
  objectInteract.material.uniforms.nodes.value = state.nodesTexture;
  objectInteract.material.needsUpdate = true;
  canvasRenderer.setRenderTarget(renderTarget);
  canvasRenderer.render(sceneInteract, noopCamera);
  canvasRenderer.readRenderTargetPixels(
    renderTarget,
    0,
    0,
    state.textureSize,
    state.textureSize,
    textureBuffer
  );
  state.createAndUpdateTexture(textureBuffer, false);

  // strategy selection
  objectStrategy.material.uniforms.nodes.value = state.nodesTexture;
  objectStrategy.material.needsUpdate = true;
  canvasRenderer.render(sceneStrategy, noopCamera);
  canvasRenderer.readRenderTargetPixels(
    renderTarget,
    0,
    0,
    state.textureSize,
    state.textureSize,
    textureBuffer
  );
  state.createAndUpdateTexture(textureBuffer, state.running);

  // canvas render
  object.material.uniforms.nodes.value = state.nodesTexture;
  object.material.needsUpdate = true;
  canvasRenderer.setRenderTarget(null);
  canvasRenderer.render(scene, camera);
}

function setRenderer() {
  let canvas = document.createElement("canvas");
  let context = canvas.getContext("webgl2", { alpha: false });
  canvasRenderer = new THREE.WebGLRenderer({
    canvas: canvas,
    context: context,
  });
  canvasRenderer.setPixelRatio(window.devicePixelRatio);
  canvasRenderer.setSize(window.innerWidth, window.innerHeight);

  renderTarget = new THREE.WebGLRenderTarget(
    state.textureSize,
    state.textureSize
  );
}

function setCamera() {
  noopCamera = new THREE.Camera();

  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    0.01,
    1000
  );
  camera.position.set(0.5, 0.5, 3.0);

  controls = new OrbitControls(camera, canvasRenderer.domElement);
  controls.mouseButtons = {
    LEFT: THREE.MOUSE.ROTATE,
    MIDDLE: THREE.MOUSE.DOLLY,
  };
  controls.touches = {
    ONE: THREE.TOUCH.ROTATE,
    TWO: THREE.TOUCH.DOLLY_ROTATE,
  };
  controls.target = new THREE.Vector3(0.5, 0.5, 0.5);
  controls.maxDistance = 5.0;
  controls.minDistance = 1.0;

  controls.update();
}

function setRtt() {
  let quad = new THREE.PlaneBufferGeometry(2, 2); // <-1,1>

  if (sceneInteract) {
    sceneInteract.dispose();
  }
  sceneInteract = new THREE.Scene();

  let interactShaderProgram = new THREE.ShaderMaterial({
    vertexShader: COMPUTE_VERTEX_SHADER,
    fragmentShader: INTERACT_FRAGMENT_SHADER,

    uniforms: {
      defectionAward: { value: state.defectionAward },
      verticesPerEdge: { value: state.verticesPerEdge },
      nodes: { value: state.nodesTexture },
    },
  });

  objectInteract = new THREE.Mesh(quad, interactShaderProgram);
  sceneInteract.add(objectInteract);

  if (sceneStrategy) {
    sceneStrategy.dispose();
  }

  sceneStrategy = new THREE.Scene();

  let strategyShaderProgram = new THREE.ShaderMaterial({
    vertexShader: COMPUTE_VERTEX_SHADER,
    fragmentShader: SELECT_STRATEGY_FRAGMENT_SHADER,

    uniforms: {
      verticesPerEdge: { value: state.verticesPerEdge },
      running: { value: state.running },
      nodes: { value: state.nodesTexture },
    },
  });

  objectStrategy = new THREE.Mesh(quad, strategyShaderProgram);
  sceneStrategy.add(objectStrategy);
}

function setScene() {
  if (scene) {
    scene.dispose();
  }

  scene = new THREE.Scene();
  scene.background = new THREE.Color(0x121212);

  const geometry = new THREE.BufferGeometry();

  const positions = [];
  state.nodes.forEach((node) => {
    positions.push(node.x, node.y, node.z);
  });
  geometry.setAttribute(
    "position",
    new THREE.Float32BufferAttribute(positions, 3)
  );

  geometry.computeBoundingSphere();

  let renderShaderProgram = new THREE.ShaderMaterial({
    vertexShader: RENDER_VERTEX_SHADER,
    fragmentShader: RENDER_FRAGMENT_SHADER,

    uniforms: {
      verticesPerEdge: { value: state.verticesPerEdge },
      pointsSize: { value: state.pointsSize },
      nodes: { value: state.nodesTexture },
    },
  });
  object = new THREE.Points(geometry, renderShaderProgram);
  scene.add(object);
}

function onPointsSizeChange() {
  object.material.uniforms.pointsSize.value = state.pointsSize;
}

function onDefectionAwardChange() {
  objectInteract.material.uniforms.defectionAward.value = state.defectionAward;
}

function onRunningChange() {
  objectStrategy.material.uniforms.running.value = state.running;
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  controls.update();

  canvasRenderer.setSize(window.innerWidth, window.innerHeight);
}
