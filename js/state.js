import { GUI } from "./datGUI/datGUI.js";
import * as THREE from "./threejs/Three.js";

const VERTICES_PER_EDGE_DEFAULT = 64;

export function getState(param) {
  const state = new State(VERTICES_PER_EDGE_DEFAULT);
  initGui(state, param);

  return state;
}

function initGui(state, param) {
  const gui = new GUI();
  gui.width = 300;

  let visual = gui.addFolder("Visuals");
  visual
    .add(state, "pointsSize", 1, 20)
    .onChange(param.onPointsSizeChange)
    .name("Points size");
  visual
    .add(state, "verticesPerEdge", { 16: 16, 36: 36, 64: 64, 100: 100 })
    .onChange((value) => {
      state.updateVerticesPerEdge(value);
      param.onVerticesPerEdgeChange();
    })
    .name("Vertices per edge");
  visual.open();

  let model = gui.addFolder("Model");
  model
    .add(state, "initialCooperation", 0.0, 1.0, 0.01)
    .name("Initial cooperation");
  model
    .add(state, "defectionAward", 0.0, 3.0, 0.01)
    .onChange(param.onDefectionAwardChange)
    .name("Defection award");
  model.add(state, "running").onChange(param.onRunningChange).name("Running");
  model.add(state, "recreate").name("Setup");
  model.open();

  let info = gui.addFolder("Info");
  info.add(state, "totalCount").name("Total nodes").listen();
  info.add(state, "ratio").name("Ratio").listen();
  info.add(state, "ticks").name("Ticks").listen();
  info
    .add({ onclick: () => state.downloadCsv() }, "onclick")
    .name("Download CSV history");
  info.open();

  let source = gui.addFolder("Source");
  source
    .add(
      {
        onclick: () =>
          window.open(
            "https://gitlab.com/horca23/th-prisoner-dilemma",
            "_blank"
          ),
      },
      "onclick"
    )
    .name("GitLab");
}

class State {
  constructor(verticesPerEdge) {
    this.pointsSize = 2.5;
    this.verticesPerEdge = verticesPerEdge;
    this.initialCooperation = 0.85;
    this.defectionAward = 1.3;
    this.running = false;

    // internals
    this.ticks = 0;
    this.history = "";
    this.ratio = "";
    this.textureSize = verticesPerEdge * Math.sqrt(verticesPerEdge);
    this.nodes = this.generateNodes(verticesPerEdge);
    this.nodesTexture = this.createTexture(
      this.createRandomTextureData(verticesPerEdge),
      true
    );
  }

  updateVerticesPerEdge(verticesPerEdge) {
    this.verticesPerEdge = verticesPerEdge;
    this.textureSize = verticesPerEdge * Math.sqrt(verticesPerEdge);
    this.recreate();
  }

  recreate() {
    this.history = "";
    this.nodes = this.generateNodes(this.verticesPerEdge);
    this.nodesTexture = this.createTexture(
      this.createRandomTextureData(this.verticesPerEdge),
      true
    );
  }

  generateNodes(verticesPerEdge) {
    if (!verticesPerEdge) {
      verticesPerEdge = this.verticesPerEdge;
    }
    let nodes = [];
    for (let i = 0; i < verticesPerEdge; i++) {
      for (let j = 0; j < verticesPerEdge; j++) {
        for (let k = 0; k < verticesPerEdge; k++) {
          nodes.push(new Node(i, j, k));
        }
      }
    }
    this.totalCount = nodes.length;
    this.ticks = 0;
    return nodes;
  }

  createRandomTextureData(verticesPerEdge) {
    let length = Math.pow(verticesPerEdge, 3) * 4;
    let data = [];
    for (let i = 0; i < length; i += 3) {
      let random = Math.random() > this.initialCooperation ? 0 : 255;
      data.push(random, random, 0, 0);
    }

    return Uint8Array.from(data);
  }

  createTexture(data, shouldUpdateHistory) {
    if (shouldUpdateHistory) {
      this.calculateRatioAndUpdateHistory(data);
    }
    let texture = new THREE.DataTexture(
      data,
      this.textureSize,
      this.textureSize,
      THREE.RGBAFormat,
      THREE.UnsignedByteType
    );
    texture.minFilter = texture.magFilter = THREE.LinearFilter;
    return texture;
  }

  createAndUpdateTexture(data, shouldUpdateHistory) {
    this.nodesTexture = this.createTexture(data, shouldUpdateHistory);
  }

  calculateRatioAndUpdateHistory(data) {
    let cs = 0;
    let ds = 0;
    for (let i = 0; i < data.length; i += 4) {
      if (data[i] !== 0) {
        cs++;
      } else {
        ds++;
      }
    }
    let csPercentage = getPercentage(cs, data.length / 4);
    let dsPercentage = getPercentage(ds, data.length / 4);
    this.ratio = `C:${csPercentage} %, D:${dsPercentage} %`;
    this.history += `${this.ticks},${cs},${ds},${this.defectionAward}\n`;
  }

  downloadCsv() {
    console.log(this);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(
      new Blob([this.history], { type: "text/csv" })
    );
    a.download = "prisoner-dilemma.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

function getPercentage(count, totalCount) {
  return ((count / totalCount) * 100).toFixed(2);
}

class Node {
  constructor(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}
